import Vue from 'vue'
import VueRouter from 'vue-router'

import CreateUserDemo from './pages/user/CreateUserDemo.vue'
import UserListDemo from './pages/user/UserListDemo.vue'
import UserProfile from './pages/user/UserProfile.vue'

import BookList from './pages/library/BookList.vue';
import AddBook from './pages/library/AddBook.vue';

import HomeworkList from './pages/groups/HomeworkList.vue';

import GroupList from './pages/groups/GroupList.vue';
import DetailGroup from './pages/groups/DetailGroup.vue';
import CreateGroup from './pages/groups/CreateGroup.vue';

import UserAuth from './pages/auth/UserAuth.vue';
import NotFound from './pages/NotFound.vue';

import store from './store/index.js';

Vue.use(VueRouter)

const routes = [{
    path: '/auth',
    component: UserAuth,
    meta: {
      requiresUnauth: true
    }
  },
  {
    path: '/user-profile',
    component: UserProfile,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/list-user',
    component: UserListDemo,
    meta: {
      requiresAuth: true,
      requiresAdmin: true,
    }
  },
  {
    path: '/create-user',
    component: CreateUserDemo,
    meta: {
      requiresAuth: true,
      requiresAdmin: true,
    }
  },
  {
    path: '/library',
    component: BookList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/add-book',
    component: AddBook,
    meta: {
      requiresAdmin: true,
      requiresAuth: true
    }
  },
  {
    path: '/group',
    component: GroupList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/group/:id',
    component: DetailGroup,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/create-group',
    component: CreateGroup,
    meta: {
      requiresAuth: true,
      requiresAdmin: true,
    }
  },
  {
    path: '/group/:id/homework',
    component: HomeworkList,
    meta: {
      requiresAuth: true
    }
  },
  {
    path: '/:notFound(.*)',
    component: NotFound
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

router.beforeEach(function (to, _, next) {
  var authUser = localStorage.getItem('authUser');
  authUser = JSON.parse(authUser);

  if (to.meta.requiresAuth && !store.getters['auth/isAuthenticated'] && !authUser) {
    next('/auth')
  } else if (to.meta.requiresUnauth && authUser) {
    next('/user-profile')
  } else if (to.meta.requiresAdmin && authUser.roles !== 'ROLE_ADMIN'){
    next('/user-profile')
  } else {
    next();
  }
})

export default router
