// 1
import gql from 'graphql-tag'

export const GetAuthors = gql`query {
    authors {
      name
      books{
        name
        category{name}
        publishedAt
      }
    }
  }
`