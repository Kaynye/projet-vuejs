// 1
import gql from 'graphql-tag'

// 2
export const GetListClassrooms = gql`query {
  classrooms{
    value : id
    text : name
  }
}
`
