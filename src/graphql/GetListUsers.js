// 1
import gql from 'graphql-tag'

// 2
export const GetListUsers = gql `
    query {
      users{
        id
        firstname
        lastname
        email
        createdAt
        class
          {
            name
          }
}
`
