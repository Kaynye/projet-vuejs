// 1
import gql from 'graphql-tag'

// 2
export const CreateUser = gql`mutation (
  $firstname : String!,
  $lastname : String!, 
  $role : String!, 
  $email : String!, 
  $password : String!, 
  $classroom : ClassroomCreateManyWithoutElevesInput!, 
  ) {
    createUser(
      firstname: $firstname
      lastname: $lastname
      role: $role
      email: $email
      password: $password
      class: $classroom
      ){
        token
        user{
          id
          firstname
          lastname
        }
      }
    }
    `
    