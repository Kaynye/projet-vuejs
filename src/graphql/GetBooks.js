// 1
import gql from 'graphql-tag'

export const GetBooks = gql`query {
    books{
      id
      name
      author{
        name
      }
      category{
        name
      }
      publishedAt
    }
  }
`