export default {
    setUser(state, payload) {
        state.email = payload.email;
        state.firstName = payload.firstName; 
        state.lastName = payload.lastName;
        state.role = payload.roles;
        state.idGroups = payload.idGroups;
        state.token = payload.token;
    },
    setGroupsUser(state,payload) {
        state.groups = payload.groups;
    }
}