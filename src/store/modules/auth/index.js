import mutations from './mutations.js';
import actions from './actions.js';
import getters from './getters.js';

export default {
    namespaced: true,
    state() {
        return {
            firstName: null,
            lastName: null,
            email: null,
            role: null,
            token: null,
            idGroups: null,
            groups: null,
            tokenApollo: localStorage.getItem('appollo-token') || null 
        }
    },
    mutations,
    actions,
    getters
}