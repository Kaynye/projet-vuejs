// import { LOGIN_USER } from '../../../gql/user/index';
// import { apolloClient } from '../../../vue-apollo';


export default {
    async login(context, payload) {
        // let resp = await apolloClient.mutate({
        //     mutation: LOGIN_USER,
        //     variables: payload
        // })

        // if (resp.data!=null && resp.data!=undefined){
        //     localStorage.setItem('appollo-token',resp.data.login.token)
        // }
        const response = await fetch('https://vps820819.ovh.net:8443/authentication_token', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: payload.email,
                password: payload.password
            })
        });
        const responseData = await response.json()
        console.log(responseData)
        if(!response.ok) {
            const error = new Error(response.message || 'Failed to authenticate.');
            throw error
        }
        // }else{
        //     let resp = await apolloClient.mutate({
        //         mutation: LOGIN_USER,
        //         variables: payload
        //     })
        //     console.log(resp)
        // }

        return context.dispatch('authUser', {
            token: responseData.token,
            email: payload.email
        })
    },
    async authUser(context, payload) {
        const email = payload.email;
        const token = payload.token
        const bearer = "Bearer " + token

        const response = await fetch(`https://vps820819.ovh.net:8443/users?email=${email}`, {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });

        var reponseData = await response.json()
        if(!response.ok) {
            const error = new Error(response.message || 'Failed to authenticate.');
            throw error
        }
        reponseData = reponseData['hydra:member'][0]
        // console.log(reponseData);
        const authUser = {
            firstName: reponseData.firstname,
            lastName: reponseData.lastname,
            idGroups: reponseData.groups,
            roles: reponseData.roles[0],
            email: reponseData.email,
            token: token
        }

        localStorage.setItem('authUser', JSON.stringify(authUser));
        context.commit('setUser', authUser)
    },
    tryLogin(context) {
        var authUser = localStorage.getItem('authUser');
        authUser = JSON.parse(authUser);
        if(authUser) {
            context.commit('setUser', authUser)
        }
    },
    logout(context) {
        const userData = {
            firstName: null,
            lastName: null,
            idGroups: null,
            groups: null,
            roles: null,
            email: null,
            token: null,
        }
        localStorage.clear();
        context.commit('setUser', userData)
    },
}