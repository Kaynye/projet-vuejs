export default {
    authUser(state) {
        const user = {
            firstName: state.firstName,
            lastName: state.lastName,
            idGroups: state.idGroups,
            role: state.role,
            email: state.email,
        }
        return user
    }, 
    token(state) {
        return state.token;
    },
    isAuthenticated(state) {
        return !!state.token;
    },
    roleUser(state) {
        return state.role;
    },
    tokenApollo(state) {
        return state.tokenApollo;
    }
}