import { findId, findGroups } from '../../../utils/utils';
import { REGISTER_USER } from '../../../gql/user/index';
import { apolloClient } from '../../../vue-apollo';

export default {
    async addGroup(context, data) {
        var groups = []
        const idUser = data.id
        data.groups.map(group => {
            let urlGroup = "/groups/" + group;
            groups.push(urlGroup)
        })

        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;

        const userData = {
            groups: groups,
        }
        console.log(userData)
        const response = await fetch(`https://vps820819.ovh.net:8443/users/${idUser}`, {
            method: 'PUT',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(userData),
        });

        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        console.log(responseData);
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error
        }
    },
    async createNewUser(context, data) {
        const role = [];
        role.push(data.role)
        const userData = {
            email: data.email,
            password: data.password,
            firstname: data.firstName,
            roles: role,
            lastname: data.lastName, 
        }
        console.log(userData);
        const auth = context.rootGetters['auth/isAuthenticated']
        var response = null;
        if(auth) {
            const token = context.rootGetters['auth/token'];
            const bearer = "Bearer " + token;
            response = await fetch(`https://vps820819.ovh.net:8443/users`, {
                method: 'POST',
                headers: {
                    'Authorization': bearer,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userData),
            });
        } else {
            response = await fetch(`https://vps820819.ovh.net:8443/users`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(userData),
            });
        }
        const responseData = await response.json()
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to create!');
            throw error
        }
        // userData.roles = data.role
        // context.commit('createNewUser', userData)
    },
    async loadUsers(context) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const response = await fetch('https://vps820819.ovh.net:8443/users', {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });
        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        console.log(responseData);
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error
        }

        const users = [];
        for(const key in responseData) {
            // let id = responseData[key]['@id'];
            let idUser = findId(responseData[key]['@id'])
            let idGroups = findGroups(responseData[key].groups)
            // console.log(idUser)
            // if(responseData[key].groups.length !== 0) {
            //     console.log(idUser)
            //     for(const y in responseData[key].groups) {
            //         // const id = responseData[key].groups[y];
            //         const idGroup = findId(responseData[key].groups[y])
            //         idGroups.push(idGroup)
            //     }
            // }
            const user = {
                id: idUser,
                firstName: responseData[key].firstname,
                lastName: responseData[key].lastname,
                email: responseData[key].email,
                role: responseData[key].roles[0],
                groups: idGroups
            }
            users.push(user);
            console.log(users)
        }
        console.log(users);
        context.commit('setUsers', users);
    },
    async createNewUserApollo(_, payload) {
        console.log("j'aiiii ")
        delete payload.role;
        let data={
            firstname: payload.firstName,
            lastname: payload.lastName,
            email: payload.email,
            password: payload.password
        }
        console.log(data)

        let resp = await apolloClient.mutate({
            mutation: REGISTER_USER,
            variables: data
        })

        console.log(resp)
    }
};