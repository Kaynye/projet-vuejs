import { GET_AUTHORS, GET_BOOKS, GET_CATEGORY } from '../../../gql/library/index';
import { apolloClient } from '../../../vue-apollo';

export default {
    async loadBooks(context) {
        const response = await apolloClient.query({
            query: GET_BOOKS,
        })
        var responseData = await response.data.books;
        console.log(responseData)
        const books = []

        for(const key in responseData) {
            const book = {
                id: responseData[key].id,
                name: responseData[key].name,
                author: {
                    id: responseData[key].author.id,
                    name: responseData[key].author.name
                },
                category: {
                    id: responseData[key].category.id,
                    name: responseData[key].category.name
                }
            }
            books.push(book)
        }
        console.log(books)
        context.commit('setBooks', books)
    },
    async loadCategory(context) {
        const response = await apolloClient.query({
            query: GET_CATEGORY,
        })

        var responseData = await response.data.books;
        console.log(responseData)
        const category = []
        for(const key in responseData) {
            const ctg = {
                id: responseData[key].category.id,
                name: responseData[key].category.name
            }
            category.push(ctg)
        }
        console.log(category)
        context.commit('setCategory', category)
    },
    async loadAuthors(context) {
        const response = await apolloClient.query({
            query: GET_AUTHORS,
        })
        var responseData = await response.data.books;
        console.log(responseData)
        const authors = []

        for(const key in responseData) {
            const author = {
                id: responseData[key].id,
                name: responseData[key].name,
            }
            authors.push(author)
        }
        console.log(authors)
        context.commit('setAuthors', authors)
    },
}
