export default {
    setBooks(state, payload) {
        return state.books = payload
    },
    setCategory(state, payload) {
        return state.category = payload
    },
    setAuthors(state, payload) {
        return state.authors = payload
    }
}