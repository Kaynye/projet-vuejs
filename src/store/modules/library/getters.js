export default {
    books(state) {
        return state.books
    },
    hasBooks(state) {
        return state.books && state.books.length > 0;
    },
    category(state) {
        return state.category
    },
    hasCategory(state) {
        return state.category && state.category.length > 0;
    },
    authors(state) {
        return state.authors
    }
}