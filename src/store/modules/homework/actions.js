import { UTCtime2dateTime, findId } from '../../../utils/utils';

export default {
    async loadHomework(context) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const response = await fetch('https://vps820819.ovh.net:8443/homework', {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });
        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error
        }
        console.log(responseData);
        const homework = [];

        for(const key in responseData) {
            let idHomework = findId(responseData[key]['@id'])
            let dateTime = UTCtime2dateTime(responseData[key].createdAt)
            const hw = {
                id: idHomework,
                name: responseData[key].name,
                content: responseData[key].content,
                groups: responseData[key].groups,
                createdAt: dateTime
            }
            homework.push(hw);
        }
        console.log(homework)
        context.commit('setHomework', homework);
    },
    async addHomeWork(context, payload) {
        console.log(payload)
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const response = await fetch('https://vps820819.ovh.net:8443/homework', {
            method: 'POST',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        });

        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to add!');
            throw error
        }
    },
    async removeHomeWork(context, payload) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;

        const response = await fetch(`https://vps820819.ovh.net:8443/homework/${payload}`, {
            method: 'DELETE',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });
        var responseData = await JSON.parse(JSON.stringify(response))
        // responseData = responseData['hydra:member'];
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to remove!');
            throw error
        }
    }
}