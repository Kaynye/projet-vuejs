export default {
    homework(state) {
        return state.homework
    },
    hasHomework(state) {
        return state.homework && state.homework.length > 0;
    }
}