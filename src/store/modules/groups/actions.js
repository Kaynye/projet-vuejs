import { findId } from '../../../utils/utils';

export default {
    async createGroup(context, data) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const groupData = {
            nameGroup: data.nameGroup,
            major: data.major
        }
        
        const response = await fetch('https://vps820819.ovh.net:8443/groups', {
            method: 'POST',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(groupData),
        });
        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        // console.log(responseData);
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to create!');
            throw error
        }
    },
    async loadGroups(context) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const response = await fetch('https://vps820819.ovh.net:8443/groups', {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });
        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        // console.log(responseData);
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error
        }

        const groups = [];

        for(const key in responseData) {
            // let id = responseData[key]['@id'];
            let idGroup = findId(responseData[key]['@id'])
            const group = {
                id: idGroup,
                nameGroup: responseData[key].nameGroup,
                major: responseData[key].major,
                homework: responseData[key].homeworks,
            }
            groups.push(group);
        }
        context.commit('setGroups', groups);
    },
    async loadGroupById(context, payload) {
        const token = context.rootGetters['auth/token'];
        const bearer = "Bearer " + token;
        const response = await fetch(`https://vps820819.ovh.net:8443/groups/${payload}`, {
            method: 'GET',
            headers: {
                'Authorization': bearer,
                'Content-Type': 'application/json'
            }
        });
        var responseData = await response.json();
        responseData = responseData['hydra:member'];
        if(!response.ok) {
            const error = new Error(responseData.message || 'Failed to fetch!');
            throw error
        }

        console.log(responseData)
    },

};