export default {
    groups(state) {
        return state.groups
    },
    hasGroups(state) {
        return state.groups && state.groups.length > 0;
    }
};