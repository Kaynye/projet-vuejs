import Vue from 'vue'
import Vuex from 'vuex'
import groupsModule from './modules/groups/index.js'
import usersModule from './modules/users/index.js'
import authModule from './modules/auth/index.js';
import homeworkModule from './modules/homework/index.js';
import libraryModule from './modules/library/index.js'

Vue.use(Vuex)

const store = new Vuex.Store({
    modules: {
        groups: groupsModule,
        users: usersModule,
        auth: authModule,
        homework: homeworkModule,
        library: libraryModule
    }
});

export default store;