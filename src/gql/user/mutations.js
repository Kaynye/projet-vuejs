import gql from 'graphql-tag'

export const REGISTER_USER = gql`
mutation (
  $firstname : String!,
  $lastname : String!, 
  $email : String!, 
  $password : String!, 
  ) {
    register(
      firstname: $firstname
      lastname: $lastname
      email: $email
      password: $password
      ){
        token
        user {
          id
          firstname
          lastname
            }
        }
    }`

export const LOGIN_USER = gql`
mutation (
  $email : String!, 
  $password : String!, 
  ) {
    login(
      email: $email
      password: $password
      ){
        token
        user {
          id
          firstname
          lastname
            }
        }
    }`