import gql from 'graphql-tag'

export const GET_BOOKS = gql`query {
  books {
    id
    name
    category {
      id
      name
    }
    author {
      id
      name
    }
    publishedAt
    reservations {
      id
    }
  }
}`

export const GET_CATEGORY = gql`query{
  books {
    category {
      id
      name
    }
    publishedAt
    reservations {
      id
    }
  }
}`

export const GET_AUTHORS = gql`query{
  authors {
    id
    name
    books {
      id
      name
    }
  }
}`