export function UTCtime2dateTime(data) {
    let UTCtime = data
    UTCtime = UTCtime.split('T')
    let date = UTCtime[0];
    let time = UTCtime[1];
    let splitTime = time.split('+')
    time = splitTime[0];
    let dateTime = date + " " + time;
    return dateTime
}

export function findId(data) {
    let dataSplit = data.split('/');
    return dataSplit[2]
}

export function findGroups(data) {
    let idGroups = []
    if(data.length > 0) {
        for(const y in data) {
            const idGroup = findId(data[y])
            idGroups.push(idGroup)
        }
        return idGroups
    }
    return idGroups
}