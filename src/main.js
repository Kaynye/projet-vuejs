import Vue from 'vue'
import App from './App.vue';

import BaseCard from './components/ui/BaseCard.vue'
import BaseButton from './components/ui/BaseButton.vue'
import BaseBadge from './components/ui/BaseBadge.vue'
import BaseModal from './components/ui/BaseModal.vue';
import BaseDialogMessage from './components/ui/BaseDialogMessage.vue';
import BaseSpinner from './components/ui/BaseSpinner.vue'

import vAvatar from '@amaury-tobias/v-avatar'
import PortalVue from 'portal-vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  BootstrapVue,
  IconsPlugin
} from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router.js';
import store from './store/index.js';
import { VuejsDatatableFactory } from 'vuejs-datatable';

import {
  createProvider
} from './vue-apollo';

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(VuejsDatatableFactory);
Vue.component('font-awesome-icon', FontAwesomeIcon)
library.add(faUserSecret)



Vue.use(vAvatar)
Vue.use(PortalVue)

Vue.component('base-card', BaseCard)
Vue.component('base-button', BaseButton)
Vue.component('base-badge', BaseBadge)
Vue.component('base-modal', BaseModal)
Vue.component('base-dialog-message', BaseDialogMessage)
Vue.component('base-spinner', BaseSpinner)

// VuejsDatatableFactory
// .useDefaultType( false )
//     .registerTableType( 'datatable', tableType => tableType.mergeSettings( {
//         table: {
//             class:   'table table-hover table-striped',
//             sorting: {
//                 sortAsc:  '<i class="fas fa-sort-amount-up" title="Sort ascending"></i>',
//                 sortDesc: '<i class="fas fa-sort-amount-down" title="Sort descending"></i>',
//                 sortNone: '<i class="fas fa-sort" title="Sort"></i>',
//             },
//         },
//         pager: {
//             classes: {
//                 pager:    'pagination text-center',
//                 selected: 'active',
//                 li:'page-itemee'

//             },
//             icons: {
//                 next:     '<i class="fas fa-chevron-right" title="Suivant"></i>',
//                 previous: '<i class="fas fa-chevron-left" title="Precendent"></i>',
//             },
//         },
//     } ) );

new Vue({
  router,
  store,
  apolloProvider: createProvider(),
  render: h => h(App)
}).$mount('#app')
